#!~/git/simple-redirect/env/bin/python3

import json
import os
from http.server import BaseHTTPRequestHandler, HTTPServer
from functools import partial

class redirects:
    def __init__(self, data):
        self.name = data['name']
        self.type = data['type']
        self.host = data['host']
        self.target = data['target']

class theRedirector(BaseHTTPRequestHandler):
    def __init__(self, domains, redirect_objects, *args, **kwargs):
        self.domains = domains
        self.redirect_objects = redirect_objects
        super().__init__(*args, **kwargs)

    def do_GET(self):
        HostSelect = (self.headers.get('Host')).split(":")[0]
        print(HostSelect)
        print(self.domains)
        self.host = HostSelect

if __name__=="__main__":
    hostName = "localhost"
    hostPort = 8080
    #open and read JSON Data
    config_file = 'redirects.json'
    ROOT_PATH = os.path.dirname(os.path.abspath(__file__))
    json_file = os.path.join(ROOT_PATH, config_file)
    with open(json_file) as json_data:
        try:
            data = json.load(json_data)
        except Exception as e:
            print("Failed to process JSON file")
            print(e)
            exit
    
    #Get List of domains to listen for
    domains = [data['domains'][i]['name'] for i in range(len(data['domains']))]
    redirect_objects = [redirects(data['targets'][i]) for i in range(len(data['targets']))]

    handler = partial(theRedirector, domains, redirect_objects)
    theServer = HTTPServer((hostName, hostPort), handler)
    try:
        theServer.serve_forever()

    except KeyboardInterrupt:
        pass




